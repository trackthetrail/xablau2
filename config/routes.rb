Rails.application.routes.draw do
  root 'static_pages#home'
  get 'static_pages/about'

  resources :portfolios
  devise_for :users

end
